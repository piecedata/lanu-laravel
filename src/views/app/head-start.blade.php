<title>{{ $title }}</title>
@if(!empty($description))
    <meta name="description"
        content="{{ $description }}" />
@endif
