<?php

namespace Lanu;

use Illuminate\Http\Request;

class Controller
{
    public function __invoke(Request $request)
    {
        return Lanu::render(
            $request->route()->defaults['props']
        );
    }
}
