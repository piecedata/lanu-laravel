<?php

namespace Lanu;

use Closure;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\RedirectResponse as Redirect;

class Middleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (!$request->header('X-Lanu')) {
            return $response;
        }

        if ($response instanceof Redirect) {
            $redirectLocation = $response->headers->get('Location');
            $response->headers->remove('Location');
            $redirectLocation = Str::replaceFirst(config('app.url'), '', $redirectLocation);
            $response->setStatusCode(201);
            $response->setContent(json_encode([
                'redirect' => $redirectLocation ?: '/',
            ]));
        }

        return $response;
    }
}
