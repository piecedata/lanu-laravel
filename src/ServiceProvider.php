<?php

namespace Lanu;

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        $this->registerMiddleware();

        $this->publishes([
            __DIR__ . '/views' => resource_path('views/lanu'),
        ]);
    }

    protected function registerMiddleware()
    {
        $this->app[Kernel::class]->pushMiddleware(Middleware::class);
    }
}
