<?php

namespace Lanu;

use Illuminate\Support\Facades\Facade;

/**
 * @method static void share($key, $value = null)
 * @method static array getShared($key = null)
 * @method static \Lanu\Response render($props = [])
 *
 * @see \Lanu\ResponseFactory
 */
class Lanu extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ResponseFactory::class;
    }
}
