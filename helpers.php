<?php

if (!function_exists('lanu')) {
    /**
     * Lanu helper.
     *
     * @param array       $props
     *
     * @return \Lanu\ResponseFactory|\Lanu\Response
     */
    function lanu($props = [])
    {
        $instance = \Lanu\Lanu::getFacadeRoot();

        if ($props) {
            return $instance->render($props);
        }

        return $instance;
    }
}
