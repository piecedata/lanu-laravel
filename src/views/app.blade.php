<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    @include('lanu.app.head-start')
    @include('lanu.app.head-end')
  </head>
  <body>
    @include('lanu.app.body-start')
    <div id="__nuxt"></div>
    @include('lanu.app.body-end')
  </body>
</html>
