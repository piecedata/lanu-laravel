# Nuxt.js Laravel Adapter

Add in composer.json
```
"repositories": [
    {
      "type": "vcs",
      "url": "git@bitbucket.org:piecedata/lanu-laravel.git"
    }
  ]
```

`composer require piecedata/lanu-laravel`

`php artisan vendor:publish --provider="Lanu\ServiceProvider"`
