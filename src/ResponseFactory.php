<?php

namespace Lanu;

use Illuminate\Support\Arr;
use Illuminate\Contracts\Support\Arrayable;

class ResponseFactory
{
    protected $sharedData = [];

    public function share($key, $value = null)
    {
        if (is_array($key)) {
            $this->sharedData = array_merge($this->sharedData, $key);
        } else {
            Arr::set($this->sharedData, $key, $value);
        }
    }

    public function getShared($key = null)
    {
        if ($key) {
            return Arr::get($this->sharedData, $key);
        }

        return $this->sharedData;
    }

    public function render($props = [], $sharedData = [])
    {
        return new Response($props, array_merge($this->sharedData, $sharedData));
    }
}
