<?php

namespace Lanu;

use Closure;
use Illuminate\Support\Arr;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Response as ResponseFactory;
use Illuminate\Support\Facades\Route;

class Response implements Responsable
{
    protected $props;
    protected $sharedData;
    protected $viewData = [];

    public function __construct($props, $sharedData)
    {
        $this->props      = $props;
        $this->sharedData = $sharedData;
    }

    public function with($key, $value = null)
    {
        if (is_array($key)) {
            $this->props = array_merge($this->props, $key);
        } else {
            $this->props[$key] = $value;
        }

        return $this;
    }

    public function withSharedData($key, $value = null)
    {
        if (is_array($key)) {
            $this->sharedData = array_merge($this->sharedData, $key);
        } else {
            $this->sharedData[$key] = $value;
        }

        return $this;
    }

    public function withViewData($key, $value = null)
    {
        if (is_array($key)) {
            $this->viewData = array_merge($this->viewData, $key);
        } else {
            $this->viewData[$key] = $value;
        }

        return $this;
    }

    public function toResponse($request)
    {
        $separator = config('app.separator') ?? ' - ';
        $title     = $this->props['title'] ?? config('app.name');

        if (Route::currentRouteName() !== 'index') {
            $title .= $separator . config('app.name');
        }

        $this->viewData['title'] = $title;

        $data = array_merge([
            'props'     => $this->props,
            'title'     => $title,
            'url'       => $request->getRequestUri(),
            'csrfToken' => csrf_token(),
        ], $this->sharedData);

        array_walk_recursive($data, function (&$dat) {
            if ($dat instanceof Closure) {
                $dat = App::call($dat);
            }
        });

        if ($request->header('X-Lanu')) {
            return new JsonResponse($data, 200, [
                'Vary'   => 'Accept',
                'X-Lanu' => 'true',
            ]);
        }

        return ResponseFactory::view('lanu.app', $this->viewData + ['app' => $data]);
    }
}
